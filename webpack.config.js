var webpack = require('webpack')
var path = require("path");
const HtmlWebpackPlugin = require('html-webpack-plugin');

const HtmlWebpackPluginConfig = new HtmlWebpackPlugin({
    template: __dirname + '/public/index.html',
    filename: 'index.html',
    inject: 'body'
})


const ASSET_PATH = process.env.ASSET_PATH || '/public';

module.exports = {
    entry: {
        app: [__dirname + "/src/app/index.js"]
    },
    output: {
        path: __dirname + '/dist',
        filename: '[name].js',
        sourceMapFilename: '[name].js.map'
    },
    resolve: {
        modules: [__dirname, 'node_modules', __dirname+ "/public"],
        extensions: ['*','.js','.jsx']
    },
    module: {

        loaders: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: 'babel-loader'
            },
            {
                test: /\.(jpg|jpeg|gif|png|ico)$/,
                exclude: /node_modules/,
                loader: "file-loader?name=public/images/[name].[ext]"
            },
            {
                test: /\.(woff|woff2|eot|ttf|svg)$/,
                exclude: /node_modules/,
                loader: "file-loader?name=public/fonts/[name].[ext]"
            }
        ]
    },
    devServer: {
        port: 7111,
        // inline: true,
        // hot: true,
        proxy: {
            "/api/check-ip": {
                target: 'http://ip-api.com',
                pathRewrite: {'^/api/check-ip': ''},
                changeOrigin: true,
                secure: false,
                onProxyReq: function onProxyReq(proxyReq, req, res) {
                    // Log outbound request to remote target
                    console.log('-->  ', req.method, req.path, '->', proxyReq.baseUrl + proxyReq.path);
                },
                onError: function onError(err, req, res) {
                    console.error(err);
                    res.status(500);
                    res.json({error: 'Error when connecting to remote server.'});
                },
                logLevel: 'debug'
            },

            "/**": {
                target: '/index.html',
                changeOrigin: false,
                secure: false,
                logLevel: 'debug',
                bypass: function (req, res, opt) {
                    if (req.headers.accept.indexOf('image') !== -1) {
                        return req.url;
                    }

                    if (req.headers.accept.indexOf('html') !== -1) {
                        return '/index.html';
                    }

                }
            }
        }
    },

    plugins: [
        HtmlWebpackPluginConfig,
        new webpack.HotModuleReplacementPlugin(),
        new webpack.DefinePlugin({
            'process.env.ASSET_PATH': JSON.stringify(ASSET_PATH),
            'process.env.NAME': JSON.stringify("dev")
        })]
};