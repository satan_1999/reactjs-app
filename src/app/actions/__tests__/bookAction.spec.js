import * as actions from '../bookActions'
import * as ActionTypes from '../actionsTypes'


describe('actions', () => {
    it('should create an action of Create Book', () => {
        const book = 'new book'
        const expectedAction = {
            type: ActionTypes.BookActionTypes.CREATE_BOOK,
            book
        }
        expect(actions.createBook(book)).toEqual(expectedAction)
    })
})