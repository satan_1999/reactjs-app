export const BookActionTypes = {
    CREATE_BOOK: 'CREATE_BOOK',
    REMOVE_BOOK: 'REMOVE_BOOK',
    CHANGE_NAME: 'CHANGE_NAME'
}


export const IpActionTypes = {
    SET_IP_DATA: 'SET_IP_DATA',
    GET_IP_DATA: "GET_IP_DATA"
}