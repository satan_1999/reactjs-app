import {BookActionTypes} from './actionsTypes'

// ./src/actions/bookActions.js
export const createBook = (book) => {
    // Return action
    return {
        // Unique identifier
        type: BookActionTypes.CREATE_BOOK,
        // Payload
        book
    }
};

export const removeBook = (book) => {
    // Return action
    return {
        // Unique identifier
        type: BookActionTypes.REMOVE_BOOK,
        // Payload
        book
    }
};

export const changePageName = (name) => {
    // Return action
    return {
        // Unique identifier
        type: BookActionTypes.CHANGE_NAME,
        // Payload
        name
    }
};