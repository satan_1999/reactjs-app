// ./src/reducers/index.js
import { combineReducers } from 'redux';
import books from './bookReducers'
import ipReducers from './ipReducers'

export default combineReducers({
    books: books,
    ips: ipReducers
    // More reducers if there are
    // can go here
});