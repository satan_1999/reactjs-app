
export default (previousState = [], action) => {

    switch (action.type) {
        case 'SET_IP_DATA':
            return [
                Object.assign({}, {ip: action.ipData.query, timezone: action.ipData.timezone}),
                ...previousState
            ];
        default:
            return previousState;
    }
};