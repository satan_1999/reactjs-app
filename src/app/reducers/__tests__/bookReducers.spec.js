import br from '../bookReducers';

const initialState = {
    books: [],
    selectedTopicUrls: [],
    selectionFinalized: false,
};

describe('test bookReducers', () => {

    it('should have initial state', () => {
        expect(br(initialState, {})).toEqual(initialState);
    });


    it('should add new book', () => {
        expect(br([], {type: 'CREATE_BOOK', book: {title: "mocked book"}})).toEqual([
            {title: "mocked book"}
        ]);
    });

});