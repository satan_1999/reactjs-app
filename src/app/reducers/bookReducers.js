var removeBook = function (bkTitle, books) {
    return books.filter(function(bk){
        return bk.title.trim() !== bkTitle.trim()
    });
}

export default (previousState = [], action) => {
    console.log("state", previousState, "action", action.type);
    switch (action.type) {
        case 'CREATE_BOOK':
            return [
                ...previousState,
                Object.assign({}, action.book)
            ];
        case 'REMOVE_BOOK':
            console.log("action.book.title", action.book.title);
            var result =  removeBook(action.book.title, previousState);
            console.log(result);
            return result;
        default:
            return previousState;
    }
};