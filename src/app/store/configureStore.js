// ./src/store/configureStore.js
import {createStore, applyMiddleware} from 'redux';
import combinedReducers from '../reducers/combinedReducers';
import logMiddleware from '../middlewares/logMiddleware'
import incrementMiddleware from '../middlewares/incrementMiddleware'
import dataMiddleware from '../middlewares/dataMiddleware'


function defaultStatus() {
    return {
        pageName: "index",
        ips:[],
        books: [
            {title: 'Book A'},
            {title: 'Book B'},
            {title: 'Book C'}
        ]
    }
}


export default function configureStore(initialState) {
    return createStore(combinedReducers, initialState || defaultStatus(), applyMiddleware(logMiddleware, incrementMiddleware, dataMiddleware));
}