import React from 'react';
import {Switch, Route} from 'react-router';
import Home from './components/common/HomePage'
import About from './components/common/AboutPage'
import Book from './components/book/BookPage'
import IP from './components/common/IpPage'

class AppRoutes extends React.Component {

    render() {
        return (
            <rountes>
                <Switch>
                    <Route exact path='/' component={Home}/>
                    <Route path='/cart' component={About}/>
                    <Route path='/books' component={Book}/>
                    <Route path='/ips' component={IP}/>
                    <Route path='/about' component={About}/>
                </Switch>
            </rountes>
        )
    };
}

export default AppRoutes