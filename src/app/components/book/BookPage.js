// ./src/components/book/BookPage.js
import React from 'react';
import {connect} from 'react-redux';
import * as bookActions from '../../actions/bookActions';

class Book extends React.Component {

    constructor(props) {
        super(props);
    }

    submitBook(input) {
        this.props.createBook(input);
    }

    render() {
        let titleInput;
        let nameInput;
        return (
            <div>
                <h3>Books Page</h3>
                <ul>
                    {/*{this.props.books.map((b, i) => <li key={i}>{b.title}</li>)}*/}


                    {this.props.books.map((b, i) =>
                        <li key={i}>{b.title} <a key={i} onClick={e => {
                            e.preventDefault();
                            this.props.removeBook(b)
                        }
                        }> Remove</a></li>
                    )}

                </ul>
                <div>
                    <h3>Books Form</h3>
                    <form onSubmit={e => {
                        e.preventDefault();
                        var input = {title: titleInput.value};
                        this.props.createBook(input);
                        e.target.reset();
                    }}>
                        <input type="text" name="title" ref={node => titleInput = node}/>
                        <input type="submit"/>
                    </form>
                </div>

                <div>
                    <h3>Name Form -- {this.props.pageName}</h3>
                    <form onSubmit={e => {
                        e.preventDefault();
                        var input = {title: nameInput.value};
                        this.props.changePageName(input);
                        e.target.reset();
                    }}>
                        <input type="text" name="title" ref={node => nameInput = node}/>
                        <input type="submit"/>
                    </form>
                </div>
            </div>
        )
    }
}

// Maps state from store to props
const mapStateToProps = (state, ownProps) => {
    return {
        // You can now say this.props.books
        books: state.books,
        child: "child",
        pageName: state.pageName,
    }
};

// Maps actions to props
const mapDispatchToProps = (dispatch) => {
    return {
        // You can now say this.props.createBook
        createBook: book => dispatch(bookActions.createBook(book)),
        removeBook: book => dispatch(bookActions.removeBook(book)),
        changePageName: pageName => dispatch(bookActions.changePageName(pageName))

    }
};

// Use connect to put them together
export default connect(mapStateToProps, mapDispatchToProps)(Book);