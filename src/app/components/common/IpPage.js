// ./src/components/book/BookPage.js
import React from 'react';
import {connect} from 'react-redux';
import * as bookActions from '../../actions/bookActions';

class IP extends React.Component {

    constructor(props) {
        super(props);
    }


    render() {
        return (
            <div>
                <h3>Books</h3>
                <ul>
                    {this.props.ips.map((ip, i) => <li key={i}> {ip.ip} in {ip.timezone}</li>)}
                </ul>
                <div>
                    <h3>My IPs </h3>
                    <button onClick={e => {
                        e.preventDefault();
                        this.props.checkIp();
                    }}>Check My IP</button>

                </div>
            </div>
        )
    }
}

// Maps state from store to props
const mapStateToProps = (state, ownProps) => {
    return {
        ips: state.ips
    }
};

// Maps actions to props
const mapDispatchToProps = (dispatch) => {
    return {
        // You can now say this.props.createBook
        checkIp:() => dispatch({
            type: "GET_IP_DATA"
        })

    }
};

// Use connect to put them together
export default connect(mapStateToProps, mapDispatchToProps)(IP);