const incrementMiddleware = store => next => action => {
    if (action.type === 'INCREMENT') {
        alert(`Increment button was clicked, current state is ${store.getState()} \nI will now add to it`);
    }
    next(action);
}

export default incrementMiddleware;