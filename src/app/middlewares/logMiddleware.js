import React from 'react'


const logMiddleWare = store => next => action => {
    console.log("Middleware triggered:", action);
    next(action);
}



export default logMiddleWare


