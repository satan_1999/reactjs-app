import request from 'superagent'

const ip_url = "/api/check-ip/json";


const dataMiddleware = store => next => action => {

    switch (action.type) {
        case 'GET_IP_DATA':
            console.log("******************")
            request.get(ip_url).end(function (err, res) {
                console.log("******************3", res, res.body)
                if (!err) {
                    // request success
                    const ipData = res.body
                    store.dispatch({
                        type: 'SET_IP_DATA',
                        ipData
                    })
                } else {
                    // request timeout (or error)
                    store.dispatch({
                        type: 'ERROR_IP_DATA',
                    })
                }
            });
        default:
            break
    }

    next(action)
};

export default dataMiddleware