import configureMockStore from 'redux-mock-store'
import dataMiddleware from '../dataMiddleware'
import * as actions from '../../actions/bookActions'
import * as types from '../../actions/actionsTypes'
import request from 'superagent';
import nocker from 'superagent-nock';

const nock = nocker(request);
const middlewares = [dataMiddleware]
const mockStore = configureMockStore(middlewares)


describe('test middleware', () => {
    // afterEach(() => {
    //     nock.cleanAll()
    // })

    it('creates SET_IP_DATA when fetching ip has been done', () => {
        nock('').get('/api/check-ip/json').reply(200, {ip: "192.168.0.1"})

        const expectedActions = [{type: types.IpActionTypes.SET_IP_DATA, ipData: {ip: "192.168.0.1"}},
            {type: types.IpActionTypes.GET_IP_DATA}
        ]

        const store = mockStore({ips: []})

        store.dispatch({type: types.IpActionTypes.GET_IP_DATA})
        // return of async actions
        expect(store.getActions()).toEqual(expectedActions)

    })
})