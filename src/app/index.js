// import React from 'react';
// import ReactDOM from 'react-dom';
// import './index.css';
// import App from './App';
// import registerServiceWorker from './registerServiceWorker';
//
// ReactDOM.render(<App />, document.getElementById('root'));
// registerServiceWorker();

// ./src/index.js
// import 'babel-polyfill';
import React from 'react';
import {Provider} from 'react-redux';
import {render} from 'react-dom';
import App from './components/App'
import {BrowserRouter} from 'react-router-dom'
import ico from '/public/images/favicon.ico'
// import '../node_modules/bootstrap/dist/css/bootstrap.min.css';

import configureStore from './store/configureStore';

const store = configureStore();

render(
    (<Provider store={store}>
        <BrowserRouter><App/></BrowserRouter>
    </Provider>),
    document.getElementById('root')
);