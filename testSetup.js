'use strict';

import jsdom from 'jsdom';

global.document = jsdom.jsdom('<html><body></body></html>');
global.window = document.defaultView;
global.navigator = window.navigator;

function noop() {
    return {};
}

// prevent mocha tests from breaking when trying to require a css file
require.extensions['.css'] = noop;
require.extensions['.png'] = noop;

// -------------------------------
// Disable webpack-specific features for tests since
// Mocha doesn't know what to do with them.

['.css', '.scss', '.png', '.jpg'].forEach(ext => {
    require.extensions[ext] = () => null;
});